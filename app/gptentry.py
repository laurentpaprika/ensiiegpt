import openai

from streamlit.logger import get_logger

logger = get_logger(__name__)

def answer(question):
    logger.info(f"""question - : {question}""")
    prompt = f"""Here's a scenario
    Your name is Ensiie GPT, an AI assistant 
    
    Given this context please answers the following question:"{question}"
    """

    engine = "text-davinci-003"
    response = openai.Completion.create(
        engine=engine,
        prompt=prompt,
        temperature=0.7, top_p=0.5,
        frequency_penalty=0, presence_penalty=0,
        max_tokens=433, stop=None)
    
    answer = response.choices[0].text


    return answer


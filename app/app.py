import streamlit as st
from streamlit.logger import get_logger
from gptentry import answer

logger = get_logger(__name__)


def main():
    st.set_page_config(
        page_title="Ensiie GPT", page_icon=":rocket:")

    st.header("Ensiie :rocket:")
    message = st.text_area("user message")

    if message:
        st.write("Invoking Ensiie Gpt...")

        result = answer(message)

        st.info(result)


if __name__ == '__main__':
    main()
